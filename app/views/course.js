/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

// var gCoursesDB = {
//     description: "This DB includes all courses in system",
// courses: [{
//         id: 1,
//         courseCode: "FE_WEB_ANGULAR_101",
//         courseName: "How to easily create a website with Angular",
//         price: 750,
//         discountPrice: 600,
//         duration: "3h 56m",
//         level: "Beginner",
//         coverImage: "images/courses/course-angular.jpg",
//         teacherName: "Morris Mccoy",
//         teacherPhoto: "images/teacher/morris_mccoy.jpg",
//         isPopular: false,
//         isTrending: true
//     },
// {
//     id: 2,
//     courseCode: "BE_WEB_PYTHON_301",
//     courseName: "The Python Course: build web application",
//     price: 1050,
//     discountPrice: 900,
//     duration: "4h 30m",
//     level: "Advanced",
//     coverImage: "images/courses/course-python.jpg",
//     teacherName: "Claire Robertson",
//     teacherPhoto: "images/teacher/claire_robertson.jpg",
//     isPopular: false,
//     isTrending: true
// },
// {
//     id: 5,
//     courseCode: "FE_WEB_GRAPHQL_104",
//     courseName: "GraphQL: introduction to graphQL for beginners",
//     price: 850,
//     discountPrice: 650,
//     duration: "2h 15m",
//     level: "Intermediate",
//     coverImage: "images/courses/course-graphql.jpg",
//     teacherName: "Ted Hawkins",
//     teacherPhoto: "images/teacher/ted_hawkins.jpg",
//     isPopular: true,
//     isTrending: false
// },
// {
//     id: 6,
//     courseCode: "FE_WEB_JS_210",
//     courseName: "Getting Started with JavaScript",
//     price: 550,
//     discountPrice: 300,
//     duration: "3h 34m",
//     level: "Beginner",
//     coverImage: "images/courses/course-javascript.jpg",
//     teacherName: "Ted Hawkins",
//     teacherPhoto: "images/teacher/ted_hawkins.jpg",
//     isPopular: true,
//     isTrending: true
// },
// {
//     id: 8,
//     courseCode: "FE_WEB_CSS_111",
//     courseName: "CSS: ultimate CSS course from beginner to advanced",
//     price: 750,
//     discountPrice: 600,
//     duration: "3h 56m",
//     level: "Beginner",
//     coverImage: "images/courses/course-css.jpg",
//     teacherName: "Juanita Bell",
//     teacherPhoto: "images/teacher/juanita_bell.jpg",
//     isPopular: true,
//     isTrending: true
// },
// {
//     id: 14,
//     courseCode: "FE_WEB_WORDPRESS_111",
//     courseName: "Complete Wordpress themes & plugins",
//     price: 1050,
//     discountPrice: 900,
//     duration: "4h 30m",
//     level: "Advanced",
//     coverImage: "images/courses/course-wordpress.jpg",
//     teacherName: "Clevaio Simon",
//     teacherPhoto: "images/teacher/clevaio_simon.jpg",
//     isPopular: true,
//     isTrending: false
// }
//     ]
// }

var gObjectCourse = [];


/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function() {
    onPageLoad()
})

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

function onPageLoad() {
    callApiGetSever();

    onLoadPopular();

    onLoadTrending();
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

// CALL API GET SEVER
function callApiGetSever() {

    $.ajax({
        url: "/courses",
        type: "GET",
        dataType: "json",
        async: false,
        success: function(data) {
            console.log(data);
            gObjectCourse = data
        },
        error: function(ajaxContent) {
            console.log(ajaxContent);
        }
    })
}

//Load PopularCourses
function onLoadPopular() {

    var vPopularCourses = "";

    $(gObjectCourse).each(function(index, course) {
        for (let bI = 0; bI < course.data.length; bI++) {
            if (course.data[bI].isPopular == true) {

                vPopularCourses +=
                    `
                        <div class="col-sm-3 ">
                            <div class="card" style="width: 17rem;">

                                <img class="card-img-top" src="${course.data[bI].coverImage}" alt="Card image cap">

                                <div class="card-body">
                                    <div class="card-title">
                                        <h5 class="text-primary" style="font-size:18px"> ${course.data[bI].courseName} </h5>

                                        <label class=""><i class="far fa-clock"></i></label>
                                        <label class="ml-2"> ${course.data[bI].duration} </label>
                                        <label class="ml-3"> ${course.data[bI].level} </label>

                                    </div>

                                    <div class="card-text">
                                        <label><b>$${course.data[bI].discountPrice} </b></label>
                                        <label class="ml-1"> <s class="opacity">$${course.data[bI].price} </s> </label>
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-sm-3 mt-1">
                                            <img class="rounded-circle" src="${course.data[bI].teacherPhoto}" alt="" style="width:120%">
                                        </div>

                                        <div class="col-sm-7 mt-2">
                                            <label class=""> ${course.data[bI].teacherName} </label>
                                        </div>

                                        <div class="col-sm-2 mt-2">
                                            <i class="far fa-bookmark"></i>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    `
                $('#popular').html(vPopularCourses)
            }
        }
    })
}


//Load TrendingCourses
function onLoadTrending() {

    var vTrendingCourses = "";
    $(gObjectCourse).each(function(index, course) {

        for (let bI = 0; bI < course.data.length; bI++) {
            if (course.data[bI].isTrending == true) {

                vTrendingCourses +=
                    `
                            <div class="col-sm-3 ">
                                <div class="card" style="width: 17rem;">

                                    <img class="card-img-top" src="${course.data[bI].coverImage}" alt="Card image cap">

                                    <div class="card-body">
                                        <div class="card-title">
                                            <h5 class="text-primary" style="font-size:18px"> ${course.data[bI].courseName} </h5>

                                            <label class=""><i class="far fa-clock"></i></label>
                                            <label class="ml-2"> ${course.data[bI].duration} </label>
                                            <label class="ml-3"> ${course.data[bI].level} </label>

                                        </div>

                                        <div class="card-text">
                                            <label><b>$${course.data[bI].discountPrice} </b></label>
                                            <label class="ml-1"> <s class="opacity">$${course.data[bI].price} </s> </label>
                                        </div>
                                    </div>

                                    <div class="card-footer">
                                        <div class="row">
                                            <div class="col-sm-3 mt-1">
                                                <img class="rounded-circle" src="${course.data[bI].teacherPhoto}" alt="" style="width:120%">
                                            </div>

                                            <div class="col-sm-7 mt-2">
                                                <label class=""> ${course.data[bI].teacherName} </label>
                                            </div>

                                            <div class="col-sm-2 mt-2">
                                                <i class="far fa-bookmark"></i>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                    `
                $('#trending').html(vTrendingCourses);
            }
        }
    })

}