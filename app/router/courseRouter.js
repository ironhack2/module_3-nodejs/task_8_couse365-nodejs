//  Khai báo thư viện express
const express = require('express');

//Import courseMiddleware
const { courseMiddleware } = require('../middleware/courseMiddleware');

//Import course Controller
const { createCourse, getAllCourse, getCourseById, updateCourseById, deleteCourseById } = require('../controllers/courseController')


// Khai báo router
const courseRouter = express.Router();

// Sử dụng middleware
courseRouter.use(courseMiddleware);

courseRouter.get('/courses', courseMiddleware, getAllCourse);

courseRouter.post('/courses', courseMiddleware, createCourse);

courseRouter.get('/courses/:courseId', courseMiddleware, getCourseById);

courseRouter.put('/courses/:courseId', courseMiddleware, updateCourseById);

courseRouter.delete('/courses/:courseId', courseMiddleware, deleteCourseById);


//Export
module.exports = courseRouter;